/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

/*!

	@class EMemoryPool
	@brief This class represents a generic memory pool.
	@details This memory pool is made for those elements that are spammed in the scene. With this class we can preallocate a numerous  pool 
		of unused objects, and make them active or inactive in demand.
*/


#ifndef EMEMORYPOOL_HPP
#define EMEMORYPOOL_HPP

#include <vector>
#include <iostream>
#include <cassert>


/// @class T
///Type of the memory pool.
template <class T>
class EMemoryPool
{
public:
	///Bit field where we store if the element is used or not without havong any reference to its type.
	bool *in_use;

	///Initialize the memory pool with a number of elements of type T.
	/*!
		@param size Size of the memory pool. Final size of the pool will be sizeof(T) * size.
	*/
	EMemoryPool(size_t size);
	///Destructor of the memory pool.
	~EMemoryPool();

	///Is a given element in use by the memory pool?
	/*!
		@param index Index of the element to check if it is in use. 
	*/
	bool inUse(int index); 

	///Return the size in bytes of the memory pool.
	const int getSize(); 

	///Get a pointer to the element type that lives in this memory pool.
	T *getPool(); 
	///Get a pointer to the first available element in the memory pool.
	/*!
		@detailsThe first available element is the first one that is not in use.
	*/
	T *getFirstAvailable();


private:
	///Size in bytes of the pool-
	const size_t size;

	///Pointer to the type stored in the pool.
	T *pool;

	///Default initialization of the pool, none of the elements in the pool are in used at the beggining.
	void initPoolToDefault();


};

#endif // !define EMEMORYPOOL_HPP


template<class T>
inline EMemoryPool<T>::EMemoryPool(size_t size)
	: size(size)
{
	this->pool = new T[size];
	this->in_use = new bool[size];
	this->initPoolToDefault(); 
}


template<class T>
inline EMemoryPool<T>::~EMemoryPool()
{
	delete[] pool; 
	delete[] in_use;
}


template<class T>
inline bool EMemoryPool<T>::inUse(int index)
{
	return this->in_use[index];
}


template<class T>
inline const int EMemoryPool<T>::getSize()
{
	return this->size;
}


template<class T>
inline T *EMemoryPool<T>::getPool()
{
	return this->pool;
}


template<class T>
inline T * EMemoryPool<T>::getFirstAvailable()
{
	for (int i = 0; i < this->size; ++i)
	{
		if (!this->in_use[i])
		{
			std::cout << "Returned index: " << i << std::endl; 
			this->in_use[i] = true; 
			return &this->pool[i];
		}
			
	}

	assert(1!=1 && "Full pool");
}


template<class T>
inline void EMemoryPool<T>::initPoolToDefault()
{
	for (int i = 0; i < this->size; ++i)
	{
		in_use[i] = false;
	}
}


