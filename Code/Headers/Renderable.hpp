/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

/*!

	@class Renderable
	@brief This class defines the behaviour of an object when its rendered.
*/


#pragma once
#ifndef RENDERABLE_HPP
#define RENDERABLE_HPP

#include <Types.hpp>
#include <ECommonFunctions.hpp>
#include <EMainWindow.hpp>


class Renderable
{
public:
	///Default initialization for renderable objects.
	Renderable(); 
	///Initialization with a box2d body.
	/*!
		@param body Body to render, in case box2d will be used.
	*/
	Renderable(b2Body * body);

	///Own implementation of the renderable object.
	virtual void render();
	
	///Set the box2d body that will be rendered.
	/*!
		@param body Body to set and render.
	*/
	void setBody(b2Body *body);


private:
	b2Body *b2_body = nullptr;

};

#endif // !define RENDERABLE_HPP
