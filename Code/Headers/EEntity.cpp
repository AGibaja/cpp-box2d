#include "EEntity.hpp"
#include <EMainWindow.hpp>
#include <ECommonFunctions.hpp>
#include <EBody.hpp>



EEntity::EEntity()
{
}


EEntity::EEntity(std::shared_ptr<EPhysicsWorld> physics_world, b2BodyDef &body_def, b2FixtureDef &fixture_def, b2Shape *shape)
	: body_def(body_def), physics_world(physics_world)
{
}


EEntity::EEntity(std::shared_ptr<EPhysicsWorld> physics_world, std::shared_ptr<EBody> body)
	: physics_world(physics_world), child_body(body)
{
	this->b2_body = body->getBody();
	this->body_shape = body->getShape();
	Renderable::setBody(this->b2_body);
}


void EEntity::update()
{
}


void EEntity::setBodyDef(b2BodyDef & body_def)
{
	this->body_def = body_def;
}


void EEntity::setBody(b2Body * body)
{
	this->b2_body = body; 
}


void EEntity::setBodyShape(b2Shape * shape)
{
	this->body_shape = shape; 
}


b2BodyDef & EEntity::getBodyDef()
{
	return this->body_def;
}

b2Body * EEntity::getB2Body()
{
	return this->b2_body;
}


const b2Shape * EEntity::getBodyShape()
{
	return this->body_shape;
}


std::shared_ptr<EBody> EEntity::getBody()
{
	return this->child_body;
}


std::shared_ptr<EPhysicsWorld> EEntity::getPhysicsWorld()
{
	return this->physics_world;
}

