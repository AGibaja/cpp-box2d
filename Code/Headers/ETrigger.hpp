/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

/*!

	@class ETrigger
	@brief This class defines the behaviour for a trigger. 
	@details A trigger is considered a box2d shape, which if some other rigidbody intersects an action will be performed.
*/


#ifndef EDETECTABLE_HPP
#define EDETECTABLE_HPP

#include <EEntity.hpp>


class ETrigger: public Updateable
{
public:
	///Default initialization for the trigger.
	ETrigger();
	///Initialize the trigger and attach it to an entity.
	/*!
		@param entity Entity to attach this trigger.
	*/
	ETrigger(EEntity *entity);

	///What to do on collision.
	virtual void onCollision() = 0; 

	///Set the entity attached to this trigger.
	/*!
		@param entity Entity to attach this trigger.
	*/
	void setEntity(EEntity* entity);


	///Set the entity attached to this trigger and intialize it.
	/*!
		@param entity Entity to attach this trigger.
	*/
	void setEntityAndInit(EEntity* entity);

	///What to do on update (in this case check for collisions and run onCollision() )
	void update() override; 


private:
	///Entity attached to this trigger.
	EEntity *entity_attached = nullptr;


	///Initalize the trigger based on its attached entity.
	void init();
	///Update the position of this trigger, according to the position of the entity attached.
	void updatePosition(); 
	

};

#endif