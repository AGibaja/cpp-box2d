#include "Renderable.hpp"


Renderable::Renderable()
{
}


Renderable::Renderable(b2Body * body)
	: b2_body(body)
{
}


void Renderable::render()
{
	const b2Transform & body_transform = b2_body->GetTransform();

	for (b2Fixture *fixture = this->b2_body->GetFixtureList(); fixture != nullptr; fixture = fixture->GetNext())
	{
		switch (fixture->GetShape()->GetType())
		{
		case (b2Shape::e_polygon):
		{
			b2PolygonShape *box2d_polygon = dynamic_cast<b2PolygonShape*>(fixture->GetShape());

			sf::ConvexShape sfml_polygon;
			sfml_polygon.setOutlineColor(sf::Color::Red);
			sfml_polygon.setOutlineThickness(1.f);

			sfml_polygon.setFillColor(sf::Color::Transparent);

			int vertices_count = box2d_polygon->m_count;

			sfml_polygon.setPointCount(vertices_count);

			for (int i = 0; i < vertices_count; ++i)
			{
				sfml_polygon.setPoint
				(
					i,
					Conversions::box2d_position_to_sfml_position(b2Mul(body_transform, box2d_polygon->m_vertices[i]), EMainWindow::window->getSize().y)
				);
			}

			EMainWindow::window->draw(sfml_polygon);
			break;
		}

		case (b2Shape::e_edge):
		{
			b2EdgeShape * edge = dynamic_cast<b2EdgeShape *>(fixture->GetShape());

			b2Vec2 start = b2Mul(body_transform, edge->m_vertex1);
			b2Vec2 end = b2Mul(body_transform, edge->m_vertex2);

			sf::Vertex line[] =
			{
				sf::Vertex(Conversions::box2d_position_to_sfml_position(start, EMainWindow::window->getSize().y), sf::Color::Green),
				sf::Vertex(Conversions::box2d_position_to_sfml_position(end, EMainWindow::window->getSize().y), sf::Color::Green),
			};

			EMainWindow::window->draw(line, 2, sf::PrimitiveType::Lines);
			break;
		}


		case (b2Shape::e_circle):
		{
			b2CircleShape * circle = dynamic_cast<b2CircleShape *>(fixture->GetShape());

			float  radius = circle->m_radius;
			b2Vec2 center = circle->m_p;

			sf::CircleShape shape;

			shape.setPosition(Conversions::box2d_position_to_sfml_position(b2Mul(body_transform, center), EMainWindow::window->getSize().y) -
				sf::Vector2f(radius, radius));
			shape.setFillColor(sf::Color::Blue);
			shape.setRadius(radius);

			EMainWindow::window->draw(shape);
			break;
		}
		}
	}
}


void Renderable::setBody(b2Body *body)
{
	this->b2_body = body;
}
