/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

/*!

	@class EParticle
	@brief This class represents a particle.
	@details A particle is an element that will be spanned multiple times, and is used for the representation of fire, electricity...
		That kind of natural phenomena.
*/


#ifndef EPARTICLE_HPP
#define EPARTICLE_HPP

#include <EEntity.hpp>


class EParticle : public EEntity
{
public: 
	EParticle();
	///Initalize a particle passing the world position size and rotation.
	/*!
		@param p_world Physics world of this particle.
		@param position Position of this particle.
		@param size Size of this particle.
		@param rotation Rotation of this particle.
	*/
	EParticle(std::shared_ptr<EPhysicsWorld> p_world, b2Vec2 position, b2Vec2 size, float rotation = 0);

	///Initialize particle wirh a position rotation and size.
	/*!
		@param position Position of the particle.
		@param position Size of the particle.
		@param position Rotation of the particle.
	*/
	void initParticle(b2Vec2 position, b2Vec2 size, float rotation = 0);
	///Set the initialized state of the particle.
	/*!
		@param p_inited Set the initialization state of the particle.
	*/
	void particleInited(bool p_inited);
	///Set the particle to dead or alive.
	/*!
		@param is_dead State of the particle, alive or dead.
	*/
	void isDead(bool is_dead); 
	///Set the velocity of the particle.
	/*!
		@param velocity Velocity of the particle.
	*/
	void setVelocity(float velocity); 
	///Set the lifespan for this particle.
	/*!
		@life_span How long woll the particle live?
	*/
	void setLifespan(float life_span); 
	///Set the rotation for this particle.
	/*!
		@param rotation Rotation of this particle.
	*/
	void setRotation(float rotation);
	///Set the physics world where this particle belongs to.
	/*!
		@param p_world Physics world for this particle.
	*/
	void setPhysicsWorld(std::shared_ptr<EPhysicsWorld> p_world);

	///Is the particle inited?
	bool particleInited();
	///Is the particle dead?
	bool isDead();

	///Get the velocity of the particle.
	float getVelocity(); 
	///Get the lifespan of the particle.
	float getLifespan(); 
	///Get the rotation of the particle.
	float getRotation(); 

	///Get the position of the particle.
	b2Vec2 getPosition(); 
	///Get the size of the particle.
	b2Vec2 getSize(); 


private:
	///Is the particle initialized.
	bool particle_inited = false;
	///Is the particle dead?
	bool is_dead = false;

	///Particle speed velocity.
	float velocity = 0.f;
	///Life span in seconds.
	float life_span = 5.f;
	///Rotation of the particle.
	float rotation = 0.f;
	
	///Position of the particle.
	b2Vec2 position = b2Vec2(0, 0);
	///Size in half extents.
	b2Vec2 size = b2Vec2(0, 0);


	///Initialize box 2d body.
	void initBody();
	

};

#endif 
