#include <EScene.hpp>
#include <EEntity.hpp>
#include <iostream>
#include <EPhysicsWorld.hpp>
#include <ECommonFunctions.hpp>
#include <EElevator.hpp>
#include <EParticleEmitter.hpp>
#include <EBody.hpp>

using namespace Conversions;


EScene::EScene(std::shared_ptr<EPhysicsWorld> p_world)
	: physics_world(p_world)
{
	EParticleEmitter emitter = EParticleEmitter(300, p_world);

	//auto p = emitter.createParticle();

	///////////////////////////////////////////////////////
	///CIRCLE CREATION
	b2CircleShape *circle = new b2CircleShape();
	circle->m_radius = 10.f;

	std::shared_ptr<EBody> circle_body = std::make_shared<EBody>(p_world, circle, 0.01f, 0.0f, 10.0f, b2Vec2(25, 500), 0, true);
	circle_entity = std::make_shared<EEntity>(p_world, circle_body);
	this->addEntity(circle_entity);
	///////////////////////////////////////////////////////



	///////////////////////////////////////////////////////
	///FLOORS CREATION
	b2EdgeShape *edge = new b2EdgeShape();
	edge->Set(b2Vec2(0, 450), b2Vec2(50, 400));

	std::shared_ptr<EBody> edge_body = std::make_shared<EBody>(p_world, edge, b2Vec2(0, 0), 0, false);
	std::shared_ptr<EEntity> edge_entity = std::make_shared<EEntity>(p_world, edge_body);
	this->addEntity(edge_entity);


	b2EdgeShape *edge2 = new b2EdgeShape();
	edge2->Set(b2Vec2(50, 400), b2Vec2(100, 450));

	std::shared_ptr<EBody> floor2_body = std::make_shared<EBody>(p_world, edge2, b2Vec2(0, 0), 0, false);
	std::shared_ptr<EEntity> floor2_entity = std::make_shared<EEntity>(p_world, floor2_body);
	this->addEntity(floor2_entity);


	b2EdgeShape *edge3 = new b2EdgeShape();
	edge3->Set(b2Vec2(225, 290), b2Vec2(350, 290));

	std::shared_ptr<EBody> floor3_body = std::make_shared<EBody>(p_world, edge3, b2Vec2(0, 0), 0, false);
	std::shared_ptr<EEntity> floor3_entity = std::make_shared<EEntity>(p_world, floor3_body);
	this->addEntity(floor3_entity);


	b2EdgeShape *edge4 = new b2EdgeShape();
	edge4->Set(b2Vec2(350, 290), b2Vec2(450, 350));

	std::shared_ptr<EBody> floor4_body = std::make_shared<EBody>(p_world, edge4, b2Vec2(0, 0), 0, false);
	std::shared_ptr<EEntity> floor4_entity = std::make_shared<EEntity>(p_world, floor4_body);
	this->addEntity(floor4_entity);


	b2EdgeShape *edge5 = new b2EdgeShape();
	edge5->Set(b2Vec2(450, 350), b2Vec2(550, 350));

	std::shared_ptr<EBody> floor5_body = std::make_shared<EBody>(p_world, edge5, 0, 0, 100, b2Vec2(0, 0), 0, false);
	std::shared_ptr<EEntity> floor5_entity = std::make_shared<EEntity>(p_world, floor5_body);
	this->addEntity(floor5_entity);


	b2EdgeShape *edge6 = new b2EdgeShape();
	edge6->Set(b2Vec2(0, 190), b2Vec2(550, 190));

	std::shared_ptr<EBody> floor6_body = std::make_shared<EBody>(p_world, edge6, 0, 0, 100, b2Vec2(0, 0), 0, false);
	std::shared_ptr<EEntity> floor6_entity = std::make_shared<EEntity>(p_world, floor6_body);
	this->addEntity(floor6_entity);

	///////////////////////////////////////////////////////


	///////////////////////////////////////////////////////
	///ELEVATORS CREATION
	std::shared_ptr<EElevator> elevator = std::make_shared<EElevator>(p_world, b2Vec2(210, 220));
	this->addEntity(elevator);
	std::shared_ptr<EElevator> elevator2 = std::make_shared<EElevator>(p_world, b2Vec2(665, 115));
	this->addEntity(elevator2);
	///////////////////////////////////////////////////////


}


void EScene::update()
{
	for (auto &e : entities)
	{
		e->update();
	}

}


void EScene::render()
{
	for (auto &e : entities)
	{
		e->render();
	}
}


void EScene::addEntity(std::shared_ptr<EEntity> entity)
{
	this->entities.push_back(entity);
}


std::vector<std::shared_ptr<EEntity>> EScene::getEntities()
{
	return this->entities;
}


std::shared_ptr<EPhysicsWorld> EScene::getPhysicsWorld()
{
	return this->physics_world;
}


std::shared_ptr<EEntity> EScene::getCircleEntity()
{
	return this->circle_entity;
}

