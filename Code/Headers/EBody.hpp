/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

/*!

	@class EBody
	@brief Encapsulation of a Box2D body and some other related elements into a class.
	@details
	Allows a flexible and comfortable use of the box2d bodies. It can be initialized several ways.
*/


#ifndef EBODY_HPP
#define EBODY_HPP

#include <Types.hpp>


class EEntity; 
class EPhysicsWorld;

class EBody
{
public: 
	///Defines a body and adds it to the allready existing box world via EPhysicsWorld.
	/*!
		@param p_world Physics world to add the bodies, and from where to get them.
		@param body_def Box2D body definition to use to create the body.
		@param fixture_def fixture to add to he Box2D body.
	*/
	EBody(std::shared_ptr<EPhysicsWorld> p_world, b2BodyDef &body_def, b2FixtureDef &fixture_def, b2Shape *shape);
	///Defines a body and adds it to the allready existing box world via EPhysicsWorld given some parameters.
	/*!
		@details It also sets the body to a position and angle from a given box2d shape.
		@param p_world Physics world to add the bodies, and from where to get them.
		@param shape Box2D body shape.
		@param position Position of the body in our own scene.
		@param angle Angle of the body.
		@param dynamic If declared as true, a box2d dynamic object will bre created. Otherwise, a static object is created.
	*/
	EBody(std::shared_ptr<EPhysicsWorld> p_world, b2Shape *shape, b2Vec2 position = b2Vec2(50, 50), float angle = 0,
		bool dynamic = false);
	///Defines a body and adds it to the allready existing box world via EPhysicsWorld given some parameters.
	/*!
		@details It also sets the body to a position and angle from a given box2d shape, and allows to specify friction, density and
			restitution.
		@param p_world Physics world to add the bodies, and from where to get them.
		@param body_def Box2D body definition to use to create the body.
		@param shape Box2D body shape.
		@param density Density of the Box2D body.
		@param restitution Restitution of the Box2D body.
		@param friction Friction of the body in our own scene.
		@param position Position of the body in our own scene.
		@param angle Angle of the body.
		@param dynamic If declared as true, a box2d dynamic object will bre created. Otherwise, a static object is created.
	*/
	EBody(std::shared_ptr<EPhysicsWorld> p_world, b2Shape *shape, float density, float restitution = 0.1f, float friction = 0.5f,
		b2Vec2 position = b2Vec2(50, 50), float angle = 0, bool is_dynamic = false);

	///Is this body dynamic?
	bool isDynamic(); 

	///Get the body angle.
	float getAngle(); 
	///Get the body's density.
	float getDensity(); 
	///Get the body's friction.
	float getFriction(); 

	///Get the box2d shape.
	const b2Shape *getShape();

	///Get the box2d body.
	b2Body *getBody();

	///Get the parent entity owner of this body.
	EEntity *getParent(); 

	///Get the position of the body.
	b2Vec2 getPosition();


private:
	///Is the body dynamic?
	bool is_dynamic = false;

	///Angle of the body.
	float angle; 
	///Density of the body.
	float density = 0.f;
	///Friction of the body.
	float friction = 1.f;
	
	///Box2D shape of the body.
	const b2Shape *shape = nullptr; 
	
	///Box 2D body.
	b2Body *box_body = nullptr; 

	///Entity owner of this object.
	EEntity *parent = nullptr; 

	///Position of the object.
	b2Vec2 position;  

	///Own world where the object belongs.
	std::shared_ptr<EPhysicsWorld> p_world = nullptr;

	///Initialize the body.
	void init();
	///Initialize the body def for the box2d body.
	void initBodyDef(); 
	///Initialize the body fixture for the box2d body's fixture. 
	void initFixture(); 

};

#endif // !define EBODY_HPP
