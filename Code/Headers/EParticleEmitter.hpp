/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

/*!

	@class EParticleEmitter
	@brief This class represents a particle emitter.
	@details This is the way we have of emitting EParticle (s) to the scene. It's also in charge of initializing the particles
		with deirsed values.
*/

#ifndef EPARTICLEEMITTER_HPP
#define EPARTICLEEMITTER_HPP

#include <EMemoryPool.hpp>
#include <EParticle.hpp>

using ParticleEmitter = EMemoryPool<EParticle>;


class EParticleEmitter : EMemoryPool <EParticle>
{
public:
	///Set the particles emitter to a given size and assign it to a physics world.
	/*!
		@param size Size of the particle emitter in bytes.
		@param p World to where this emitter belongs to.
	*/
	EParticleEmitter(size_t size, std::shared_ptr<EPhysicsWorld> p);

	///Render all particles in the emitter.
	void renderParticles(); 

	///Create a particle to emit it.
	EParticle *createParticle();


private:
	///World of the particle emitter.
	std::shared_ptr<EPhysicsWorld> p_world = nullptr;

};

#endif // !define EPARTICLEEMITTER_HPP
