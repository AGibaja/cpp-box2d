/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

/*!

	@class EScene
	@brief This class represents the main scene, and holds a vector of entities.
*/

#pragma once
#ifndef ESCENE_HPP
#define ESCENE_HPP

#include <EEntity.hpp>
#include <EPhysicsWorld.hpp>
#include <vector>


class EScene : public Updateable, public Renderable
{
public:
	///Default initalize the scene, initializinf the physics world.
	EScene(std::shared_ptr<EPhysicsWorld> p_world); 

	///Own implementation of the scene update.
	void update() override; 
	///Render all elements in the scene.
	void render() override; 

	///Add an entity to the entity vector of  this scene.
	/*!
		@param entity Entity to add to the entities vector.
	*/
	void addEntity(std::shared_ptr<EEntity> entity);

	///Get the vector of entities in the scene.
	std::vector <std::shared_ptr<EEntity>> getEntities();

	///Get the pointer to the EPhysicsWorld of attached to this scene.
	std::shared_ptr<EPhysicsWorld> getPhysicsWorld(); 

	///Get the movable circle of the scene.
	std::shared_ptr<EEntity> getCircleEntity();

private:
	///Pointer to the physics world of this scene.
	std::shared_ptr<EPhysicsWorld> physics_world = nullptr;

	///Movable entity of the scene.
	std::shared_ptr <EEntity> circle_entity = nullptr;

	///Vector of entities of the scene.
	std::vector<std::shared_ptr<EEntity>> entities;

};

#endif // !define ESCENE_HPP
