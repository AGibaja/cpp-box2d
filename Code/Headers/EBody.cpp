#include "EBody.hpp"
#include <EEntity.hpp>
#include <EPhysicsWorld.hpp>




EBody::EBody(std::shared_ptr<EPhysicsWorld> p_world, b2BodyDef &body_def, b2FixtureDef &fixture_def, b2Shape *shape)
	: p_world(p_world)
{
	this->box_body = this->p_world->getBoxWorld()->CreateBody(&body_def);
	this->shape = fixture_def.shape;
	this->box_body->CreateFixture(&fixture_def);
}


EBody::EBody(std::shared_ptr<EPhysicsWorld> p_world, b2Shape * shape, b2Vec2 position, float angle, bool dynamic)
	: p_world(p_world), shape(shape), position(position), angle(angle), is_dynamic(dynamic)
{
	this->init();
}


EBody::EBody(std::shared_ptr<EPhysicsWorld> p_world, b2Shape * shape, float density, float restitution, float friction,
	b2Vec2 position, float angle, bool dynamic)
	: p_world(p_world), shape(shape), density(density), friction(friction),
		position(position), angle(angle), is_dynamic(dynamic)
{
	this->init();
}


bool EBody::isDynamic()
{
	return this->is_dynamic;
}


float EBody::getAngle()
{
	return this->angle;
}


float EBody::getDensity()
{
	return this->density;
}


float EBody::getFriction()
{
	return this->friction;
}


const b2Shape *EBody::getShape()
{
	return this->shape;
}


b2Body *EBody::getBody()
{
	return this->box_body;
}


EEntity * EBody::getParent()
{
	return this->parent;
}


b2Vec2 EBody::getPosition()
{
	return this->position;
}


void EBody::init()
{
	this->initBodyDef();
	this->initFixture();
}


void EBody::initBodyDef()
{
	b2BodyDef def;
	def.position = this->position; 
	if (this->is_dynamic)
		def.type = b2_dynamicBody;
	else
	{
		def.type = b2_staticBody;	
	}
		

	def.position = this->position; 
	def.angle = this->angle;

	this->box_body = this->p_world->getBoxWorld()->CreateBody(&def);
}


void EBody::initFixture()
{
	b2FixtureDef fixture_def;
	fixture_def.shape = this->shape;
	fixture_def.density = this->density;
	fixture_def.restitution = 0.15f;
	fixture_def.friction = this->friction;
	this->box_body->CreateFixture(&fixture_def);

}
