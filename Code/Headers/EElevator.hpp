/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

/*!

	@class EElevator
	@brief Abstraction of a set of bodies that will represent an elevator.
	@details Using Box 2D motors and building two bodies, an elevator can be simulated.
*/


#ifndef EELEVATOR_HPP
#define EELEVATOR_HPP

#include <vector>

#include <EEntity.hpp>
#include <ETrigger.hpp>


class EElevator : public EEntity, public ETrigger, public Updateable
{
public:
	///Initialize the elevator initializing its world, the position and setting translation limits.
	/*!
		@param physics_world Physics world where its located at. @see EPhysicsWorld
		@param position Position of the elevator in the scene.
		@param translation_limits Upper and lower translation limits of the elevator. The upper limit is the first
			argument for b2Vec2, and the second one is the lower one.
	*/
	EElevator(std::shared_ptr<EPhysicsWorld> physics_world, b2Vec2 position, 
		b2Vec2 translation_limits = b2Vec2(-130, 30));
	
	///What to do on collision.
	void onCollision() override;
	///How to updare this entity.
	void update() override; 
	///Render all elements that form this body.
	void render() override;
	///Initialize the elevator.
	/*!
		@details This method initializes the elevator, creating two box2d bodies.
			Said bodies are joined by a prismatic joint with motor.
	*/
	void init();
	

	///Get the box2d joint.
	b2Joint *getJoint();


private:
	///Position of the elevator.
	b2Vec2 position; 
	///Size of the elevator.
	b2Vec2 size;
	///Upper and lower translation limits for the elevator.
	b2Vec2 translation_limits;

	///Box2d body def for the elevator.
	b2BodyDef body_def;

	///Fixture def for the elevator.
	b2FixtureDef fix_def;

	///Shape of the main platform.
	b2PolygonShape *main_platform_shape= nullptr; 
	///Shape of the axis shape, which joins the elevator with the wall.
	b2PolygonShape *axis_shape= nullptr; 

	///Prismatoc joint as a parent type.
	b2Joint *p_joint = nullptr; 

	///EBody of the main platform.
	std::shared_ptr<EBody> main_platform_body = nullptr;
	///EBody of the axis.
	std::shared_ptr<EBody> axis_body = nullptr; 

	///EEntity for the main platform.
	std::shared_ptr<EEntity> main_platform = nullptr;
	///EEntity for the axis.
	std::shared_ptr<EEntity> axis = nullptr;


	///Initialize body a, which is the main platform of the elevator.
	void initBodyA();
	///Initialzie body b, which is the axis or anchor point of the elevator.
	void initBodyB();
	///Initialize its joints properly so it has also a motor created.
	void initJoint();

};



#endif // !define EELEVATOR_HPP
