/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

/*!

	@class EPhysicsWorld
	@brief This class provides an own interface to the box2d world, with some additions.
*/


#pragma once
#ifndef EPHYSICSWORLD_HPP
#define EPHYSICSWORLD_HPP

#include <memory>
#include <Box2D/Box2D.h>

#include <Updateable.hpp>



class EPhysicsWorld
{
public: 
	///Default initialization for the phsysics world.
	/*!
		@details It initializes the box2d world from a time_step, velocity iterations, position_iterations.
	*/
	EPhysicsWorld(); 

	///Update the physics world given a delta time.
	/*!
		@delta_time Delta time for the frame.
	*/
	void update(float delta_time); 

	///Get the 
	std::shared_ptr<b2World> getBoxWorld(); 


private:
	///Box2d time step.
	float time_step = 1.0f / 60.0f;
	///Box2d velocity iterations.
	int velocity_iterations = 8;
	///Box2d position iterations.
	int position_iterations = 4; 

	///Pointer to the box2d world.
	std::shared_ptr<b2World> box_world = nullptr;

	///Create the box2d world and initialize it accordingly.
	void initWorld(); 


};
#endif // !1

