/*!
	@author �lvaro Gibaja alvarogibaja09@gmail.com
	@copyright
	Copyright 2019 �lvaro Gibaja

	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

/*!

	@class EEntity
	@brief This class represents a generic entity. 
	@details An entity holds a refrence to the EEPhysicsWorld. It also has all the information needed to create a box 2d body in said world.
		It inherits from Renderable and Updateable in this case, but it can be made more generic without the latter ones.
*/


#pragma once
#ifndef EENTITY_HPP
#define EENTITY_HPP

#include <Types.hpp>
#include <Updateable.hpp>
#include <Renderable.hpp>
#include <EPhysicsWorld.hpp>


class EBody; 


class EEntity : public Renderable, public Updateable
{
public: 
	///Default initialization for the entity.
	EEntity();
	///Initialization where the physics world is defined, with more needed box2d attributes needed to create the body.
	/*!
		@param physics_world Physics world where the entity will be created.
		@param body_def Box2D body definition to create the box2d body.
		@param fixture_def Fixture definition for the box2d body.
		@param shape Shape for the box2d body.
	*/
	EEntity(std::shared_ptr<EPhysicsWorld> physics_world, b2BodyDef &body_def, b2FixtureDef& fixture_def, b2Shape *shape);
	///Initialization where the physics world is defined, with an own box2d body abstraction.
	/*!
		@param physics_world Physics world where the entity will be created.
		@param body_def Box2D body definition to create the box2d body.
		@param fixture_def Fixture definition for the box2d body.
		@param shape Shape for the box2d body.
	*/
	EEntity(std::shared_ptr<EPhysicsWorld> physics_world, std::shared_ptr<EBody> body);

	///Update the entity.
	void update() override;

	///Setter for the box2d body definition.
	/*!
		@param body_def Body definition for the box 2d body.
	*/
	void setBodyDef(b2BodyDef &body_def);
	///Setter for the box2d body.
	/*!
		@param body Box2d body of this entity.
	*/
	void setBody(b2Body *body); 
	///Setter for the body shape.
	/*!
		@param shape Box2d shape assigned to this entity.
	*/
	void setBodyShape(b2Shape *shape);

	///Getter for the box2d body definition.
	b2BodyDef &getBodyDef();

	///Getter for the box2d body attached to this entity.
	b2Body *getB2Body(); 

	///Getter for the box2d shape of the body attached to this entity.
	const b2Shape *getBodyShape(); 

	///Getter for the box2d body attached to this entity.
	std::shared_ptr<EBody> getBody();

	///Getter of the physics world where this entity belongs to.
	std::shared_ptr<EPhysicsWorld> getPhysicsWorld(); 


protected: 
	///Body definition of the box2d body.
	b2BodyDef body_def;

	///Box2D body attached to this entity.
	b2Body *b2_body = nullptr;

	///Shape of the box2d shape attached to this entity.
	const b2Shape *body_shape = nullptr; 

	///Body own abstraction attached to this entity.
	std::shared_ptr<EBody> child_body = nullptr;

	///Physics world of this entity. @see EPhysicsWorld. 
	std::shared_ptr<EPhysicsWorld> physics_world = nullptr;


};

#endif // !EENTITY?HPP
