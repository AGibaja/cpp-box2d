#include "EPhysicsWorld.hpp"
#include <cassert>
#include <ECommonFunctions.hpp>
#include <iostream>


using namespace Conversions;


EPhysicsWorld::EPhysicsWorld()
{
	this->initWorld(); 
}


///Mirar
void EPhysicsWorld::update(float delta_time)
{
	//this->box_world->Step(delta_time + 0.009f, this->velocity_iterations, this->position_iterations);
	this->box_world->Step(delta_time , this->velocity_iterations, this->position_iterations);
}


std::shared_ptr<b2World> EPhysicsWorld::getBoxWorld()
{
	return this->box_world;
}


void EPhysicsWorld::initWorld()
{
	this->box_world = std::make_shared<b2World>(b2Vec2(0, -9.8f));
}
