#include <EParticle.hpp>

#include <EBody.hpp>


EParticle::EParticle()
	: EEntity()
{
}


EParticle::EParticle(std::shared_ptr<EPhysicsWorld> p_world, b2Vec2 position,
	  b2Vec2 size, float rotation)
	: EEntity(), position(position), rotation (rotation), size(size)
{
	this->physics_world = p_world; 
	this->initBody();
}


void EParticle::particleInited(bool p_inited)
{
	this->particle_inited = p_inited;
}


void EParticle::isDead(bool is_dead)
{
	this->is_dead = is_dead; 
}


void EParticle::setVelocity(float velocity)
{
	this->velocity = velocity;
}


void EParticle::setLifespan(float life_span)
{
	this->life_span = life_span; 
}


void EParticle::setRotation(float rotation)
{
	this->rotation = rotation; 
}


void EParticle::setPhysicsWorld(std::shared_ptr<EPhysicsWorld> p_world)
{
	this->physics_world = p_world;
}


bool EParticle::particleInited()
{
	return this->particle_inited;
}


bool EParticle::isDead()
{
	return this->is_dead;
}


float EParticle::getVelocity()
{
	return this->velocity;
}


float EParticle::getLifespan()
{
	return this->life_span;
}


float EParticle::getRotation()
{
	return this->rotation;
}


b2Vec2 EParticle::getPosition()
{
	return this->position;
}


b2Vec2 EParticle::getSize()
{
	return this->size;
}


void EParticle::initBody()
{
	this->body_def.type = b2_dynamicBody;
	this->body_def.position.Set(this->position.x, this->position.y);
	this->body_def.gravityScale = 0.5f;

	b2PolygonShape *casted = new b2PolygonShape();
	float32 x_, y_; 
	x_ = size.x;
	y_ = size.y;

	casted->SetAsBox(x_, y_);
	
	this->body_shape = casted;

	b2FixtureDef fix_def;
	fix_def.density = 0.1f; 
	fix_def.shape = this->body_shape;
	

	this->child_body = std::make_shared<EBody>(this->physics_world, this->body_def,
		fix_def, casted);

	this->b2_body = child_body->getBody();

	Renderable::setBody(this->b2_body);

}


void EParticle::initParticle(b2Vec2 position, b2Vec2 size, float rotation)
{
	this->position = position;
	this->size = size;
	this->rotation = rotation;

	this->initBody();
}
