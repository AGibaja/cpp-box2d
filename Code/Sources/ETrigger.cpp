#include <ETrigger.hpp>
#include <cassert>



ETrigger::ETrigger()
{

}


ETrigger::ETrigger(EEntity *entity)
	: entity_attached(entity)
{
	this->init();
}


void ETrigger::init()
{
	const b2Shape *shape = this->entity_attached->getBodyShape();

	b2BodyDef body_def;
	body_def.type = b2_staticBody;
	

	b2FixtureDef fix;
	fix.shape = shape;
	fix.isSensor = true;

	this->entity_attached->getB2Body()->CreateFixture(&fix);
}


void ETrigger::setEntity(EEntity * entity)
{
	this->entity_attached = entity;
}


void ETrigger::setEntityAndInit(EEntity * entity)
{
	this->setEntity(entity); 
	this->init(); 
}


void ETrigger::update()
{
	if (this->entity_attached->getB2Body()->GetContactList() != nullptr)
	{
		this->onCollision();
	}
	
}


void ETrigger::updatePosition()
{
	if (this->entity_attached == nullptr)
		assert(this->entity_attached != nullptr);
	else
	{
		//this->
	}
}



/////////////////////////////////////////////////////////
	/////TRIGGER CREATION
	///*b2PolygonShape *rectangle = new b2PolygonShape();
	//rectangle->SetAsBox(20, 40);*/

	//b2BodyDef body_def;
	//body_def.type = b2_staticBody;
	//b2Vec2 pos = r1bodydef.position + pjdef.localAnchorA; 
	//body_def.position.Set(pos.x, pos.y);

	//b2Body *trigger;

	//b2FixtureDef fix;
	//fix.shape = pa;
	//fix.filter.categoryBits = category::PLATFORM;

	//fix.isSensor = true;

	//std::shared_ptr<EBody> trigger_b = std::make_shared<EBody>(p_world, body_def, fix, pa);

	//EEntity trigger_entity = EEntity(p_world, trigger_b);
	/////////////////////////////////////////////////////////