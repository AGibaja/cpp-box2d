#include <EElevator.hpp>

#include <EBody.hpp>
#include <iostream>



EElevator::EElevator(std::shared_ptr<EPhysicsWorld> p, b2Vec2 position, b2Vec2 translation_limits)
	: position (position), translation_limits(translation_limits)
{
	this->physics_world = p; 
	this->init();
}



void EElevator::onCollision()
{
	auto prismatic_joint = (b2PrismaticJoint*)this->p_joint;
	prismatic_joint->SetMotorSpeed(prismatic_joint->GetMotorSpeed() * -1);
}


void EElevator::update()
{
	ETrigger::update(); 
}


void EElevator::render()
{
	this->main_platform->render();
	this->axis->render(); 
}


void EElevator::init()
{
	this->initBodyA();
	this->initBodyB();
	this->initJoint();
}


b2Joint * EElevator::getJoint()
{
	return this->p_joint;
}


void EElevator::initBodyA()
{
	this->body_def.type = b2_dynamicBody;
	this->body_def.position.Set(this->position.x, this->position.y);

	this->main_platform_shape = new b2PolygonShape();
	this->main_platform_shape->SetAsBox(50, 3);

	this->fix_def.density = 0;
	this->fix_def.shape = this->main_platform_shape;

	this->main_platform_body = std::make_shared<EBody>(this->physics_world, this->body_def,
		this->fix_def, this->main_platform_shape);

	this->main_platform = std::make_shared<EEntity>(this->physics_world, this->main_platform_body);

	this->setEntityAndInit(this->main_platform.get());

}


void EElevator::initBodyB()
{
	this->body_def.type = b2_staticBody;
	this->body_def.position.Set(this->body_def.position.x, this->body_def.position.y+200);

	this->axis_shape = new b2PolygonShape();
	this->axis_shape->SetAsBox(1, 1);

	this->fix_def.density = 0; 
	this->fix_def.shape = this->axis_shape;

	this->axis_body = std::make_shared<EBody>(this->physics_world, this->body_def, this->fix_def, this->axis_shape);

	this->axis = std::make_shared<EEntity>(this->physics_world, this->axis_body);

}


void EElevator::initJoint()
{
	b2PrismaticJointDef pjdef; 
	pjdef.bodyA = this->axis_body->getBody();
	pjdef.bodyB = this->main_platform_body->getBody();
	pjdef.collideConnected = false;
	pjdef.localAxisA.Set(0, 1);
	pjdef.localAxisA.Normalize();
	pjdef.localAnchorA.Set(-55, 0);
	pjdef.localAnchorB.Set(1, 0);
	pjdef.enableLimit = true;
	pjdef.lowerTranslation = this->translation_limits.x;
	pjdef.upperTranslation = this->translation_limits.y;
	pjdef.enableMotor = true;
	pjdef.maxMotorForce = 50;
	pjdef.motorSpeed = 10;

	this->p_joint = (b2PrismaticJoint*)this->physics_world->getBoxWorld()->CreateJoint(&pjdef); 
}
