#pragma once
#include <ECircle.hpp>



ECircle::ECircle()
	: EShape()
{
	this->initCircle();
}


ECircle::ECircle(Vector2 position, float radius, float rotation)
	: EShape(position, rotation), radius(radius)
{
	this->initCircle();
	this->setPosition(position);
	this->setRadius(radius);
	this->setRotation(rotation);
}


void ECircle::setPosition(Vector2 position)
{
	this->s_circle_shape->setPosition(position.x, position.y);
	this->b_circle_shape->m_p = position;
	this->position = position; 
}


void ECircle::setRotation(float rotation)
{
	this->s_circle_shape->setRotation(rotation);
	this->rotation = rotation;
}


std::shared_ptr<b2Shape> ECircle::getBoxShape()
{
	return this->b_circle_shape;
}


std::shared_ptr<sf::Shape> ECircle::getSfmlShape()
{
	return this->s_circle_shape;
}


void ECircle::setRadius(float radius)
{
	this->s_circle_shape->setRadius(radius);
	this->b_circle_shape->m_radius = radius; 
	this->radius = radius; 
}


float ECircle::getRadius()
{
	return this->radius;
}


void ECircle::initCircle()
{
	this->b_circle_shape = std::make_shared<b2CircleShape>(); 
	this->s_circle_shape = std::make_shared<sf::CircleShape>(); 
}
