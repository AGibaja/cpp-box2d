#include <EParticleEmitter.hpp>
#include <EParticle.hpp>


EParticleEmitter::EParticleEmitter(size_t size, std::shared_ptr<EPhysicsWorld> p)
	: ParticleEmitter(size), p_world(p)
{
}


void EParticleEmitter::renderParticles()
{
	for (int i = 0; i < this->getSize(); ++i)
	{
		if (!this->getPool()[i].isDead())
		{
			/*this->getPool()[i].render();
			EParticle *part = &this->getPool()[i];
			auto b2b = part->getB2Body();
			std::cout << std::endl; */
		}	
	}
}


EParticle *EParticleEmitter::createParticle()
{
	EParticle *p = this->getFirstAvailable(); 

	p->setPhysicsWorld(this->p_world);
	p->initParticle(b2Vec2(300, 300), b2Vec2(60, 60));

	return p;
}
