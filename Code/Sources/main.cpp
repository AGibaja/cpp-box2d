#include <stdio.h>
#include <iostream>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <Types.hpp>

#include <EMainWindow.hpp>
#include <EEntity.hpp>
#include <EScene.hpp>
#include <EBody.hpp>
#include <ECommonFunctions.hpp>
#include <EElevator.hpp>
#include <EParticleEmitter.hpp>


using namespace sf; 
using namespace std; 


enum category
{
	CIRCLE = 0x0001, 
	PLATFORM = 0x0002
};


void setMainWindow()
{
	EMainWindow::window = new sf::RenderWindow(sf::VideoMode(800, 600 ), "Unnamed Window");
}


inline Vector2f box2d_position_to_sfml_position(const b2Vec2 & box2d_position, float window_height)
{
	return Vector2f(box2d_position.x, window_height - box2d_position.y);
}



int main()
{
	setMainWindow(); 


	std::shared_ptr<EPhysicsWorld> p_world = std::make_shared<EPhysicsWorld>();
	EScene scene = EScene(p_world);

	
	
	Clock timer;
	float delta_time = 0.017f;

	while (EMainWindow::window->isOpen())
	{
		timer.restart();
		sf::Event event;
		while (EMainWindow::window->pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				EMainWindow::window->close();
			else if (event.type == sf::Event::KeyPressed)
			{
				switch (event.key.code)
				{
					case (Keyboard::D) :
					{
						scene.getCircleEntity()->getB2Body()->SetAngularVelocity(0);
						scene.getCircleEntity()->getB2Body()->ApplyTorque(-32500, true);
						break;
					}

					case (Keyboard::A):
					{
						scene.getCircleEntity()->getB2Body()->SetAngularVelocity(0);
						scene.getCircleEntity()->getB2Body()->ApplyTorque(32500, true);

						break;
					}

					default:
						break; 
				}
			
			}

			else if (event.type == sf::Event::KeyReleased)
			{				
				scene.getCircleEntity()->getB2Body()->SetAngularVelocity(0);
			}
		}



		EMainWindow::window->clear();


		scene.getPhysicsWorld()->update(delta_time);

		scene.update(); 
		scene.render(); 

		EMainWindow::window->display();
		delta_time = (delta_time + timer.getElapsedTime().asSeconds()) * 0.5f;


		std::cout << "T: " << delta_time << std::endl;
	}
}